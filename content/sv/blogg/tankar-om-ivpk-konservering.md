---
title: "Tankar om avsnitt 53 av I väntan på katastrofen"
date: 2022-04-19 09:15:00 +0200
categories:
 - konservering
tags:
 - konservering
 - tryckkonservering
 - pressure canning
 - tankar
---
Härom dagen lyssnade jag på senaste avsnittet av en av mina favoritpoddar: I väntan på katastrofen. Kalasbra podd som jag rekommenderar alla att lyssna på.

I avsnitt 53 så pratar man bland annat om hermetisk inkokning, alltså konservering genom att göra konservburkar. I det stora hela var avsnittet väldigt bra och går igenom saker väldigt bra. Det kom dock då och då lite detaljer jag reagerade på och som jag vill ta upp.

## Ugnskonservering

En grej är att Kalle lite snabbt nämner att om det man ska konservera har tillräckligt lågt pH (alltså är tillräckligt surt) så behöver man inte tryckkonservera det. Det här är helt korrekt, men han nämner då att det räcker med att ställa burkarna i ugnen. Det... är inte korrekt. På [healthycanning.com](https://www.healthycanning.com/oven-canning/), vilket är en blogg som är väldigt bra och hänvisar alla påståenden till seriösa källor som USDA och liknande, kan man läsa följande:

> The main problem is that the food inside jars doesn’t get hot enough consistently enough to ensure that proper heat penetration has taken place to ensure the biosafety of the product inside the jars.

> The dry heat of an oven just can’t achieve the same guaranteed result that the two authorized canning methods can. Dry air is a poor conductor of heat — in fact, air period is a poor conductor of heat (that is why pressure canners are vented first.)

Alltså: Om du ska konservera sylt, saft, marmelad, inläggningar eller annat med lågt pH, gör det i **KASTRULL** och inte i ugnen. Och ja, du *bör* konservera sylt och inläggningar. Det är inte för botulism du ska koka burkarna för i det fallet dock, utan andra inte lika *men ändå ganska* otrevliga bakterier, mögelsporer, svampar och så vidare. Du minskar alltså risken att hitta en möglig burk ett halvår senare och du gör det möjligt att förvara burken i ett rumstempererat skafferi. Du slipper också hålla på och krångla med att sterilisera burkar, ha i konserveringsmedel, vända burkar upp-och-ner och annat trams. Det är ärligt talat lättare att bara konservera och du får bättre slutresultat. Läs [här](https://www.healthycanning.com/why-do-you-have-to-process-jars-of-jam/) för mer info. Nu gick jag lite off-topic eftersom det här inlägget skulle handla om det där avsittet av den där podden... :)

## Honungsburkar

Patrick rekommenderar honungsburkar istället för "dyra burkar med packning". Han har helt rätt i att det blir rätt dyrt att köpa till exempel Weck, Le Parfait eller speciellt amerikanska Ball och kanadensiska Bernardin som ju måste importeras med tullavgift och allt vad det innebär.

I Nordamerika [avråder man](https://www.healthycanning.com/one-piece-lids-for-home-canning/) från att använda burkar med skruvlock. Jag kan se några nackdelar som gör att jag ändå föredrar mina dyra Weck-burkar (exempelvis att jag kan hålla i locket på en färdig burk och se att vakuumet är så pass starkt att locket ändå sitter fast), men jag kan också känna att jag vill plocka fram min foliehatt ibland när jag läser vad universiteten i USA kommit fram till. Det känns ju *lite* som om Ball har betalat dem för att avråda från skruvlock, men nu är jag såklart ute på djupt vatten.

Healthy Canning skriver alltså:

> In any event, the recommendation against is not based on a microbiological safety issue per se, but rather on operational issues:
> Do all one-piece lids provide sufficient venting during home canning processing to create a strong and lasting vacuum seal, and how strong does that vacuum rate in comparison to that created with two-piece lids?
> Do all one-piece lids provide a way for home canners to check on the integrity of the seal, without actually having to open the jar at a time when they don’t actually want to?

Om du tycker att du litar tillräckligt på skruvlock; att du litar på att du fått tillräckligt vakuum och att du inte är orolig för att ett år senare hitta femton mögliga burkar i skafferiet - då kan du nog göra det. Samtidigt berättar Kalle hur det alltid är en eller ett par burkar som inte får vakuum när han konserverar... Jag kan ju inte säga att jag har lika dålig statistik med mina Weck-burkar. :) Väldigt sällan jag får en burk utan vakuum.

Hur just du gör bestämmer du själv. Jag vill bara att du ska veta att det finns lite kontrovers mot att använda honungsburkar med skruvlock innan du kör igång och alla håller inte med Patrick och Kalle att det bara är att tuta och köra med honungsburkar. Min åsikt är att det antagligen är lugnt men att det är lite mer opraktiskt. Min åsikt spelar dock ingen roll och jag tycker du ska bilda din egen uppfattning. Vill du inte ta någon som helst risk så bör du köra på burkar med lock som bara sitter på med vakuum, såsom Weck, Le Parfait, Ball eller Bernardin. Burkar med så kallat patentlock funkar för övrigt också bra i min erfarenhet. Principen blir då att du **släpper på låset** när burkarna kallnat och förvarar dem olåsta. Vakuumet kommer att hålla fast locket och tydligt visa att burken är tät. Inte heller såna burkar är egentligen godkända av USDA, men jag kan ärligt talat inte se problemet. Jag tror absolut de skulle klara tester om de gjordes-

## Konservera överbliven mat

Både Kalle och Patrick berättar att de ibland slänger i överbliven mat i burkar och konserverar för att få matlådor. Det låter så himla smidigt. Samtidigt vet jag inte om jag skulle våga det själv. Varför? Jo, om du tittar på flera vetenskapligt testade recept för konservering som publicerats i USA och Kanada så kommer du märka några saker: Alla recept har lite olika luftspalt (headspace), olika lång konserveringstid, vissa ingredienser kan inte ens konserveras säkert, vissa saker tar en viss tid att konservera i tärnad form och en annan tid i puréform, vissa recept kan konserveras med stora burkar, andra bara med mindre, och så vidare och så vidare. Det verkar i alla fall som att man i Nordamerika absolut inte anser att det är någon "one size fits all" vad gäller konservering. 

Jag uppfattar det som att det är olika beroende på vad burken innnehåller hur snabbt värmen tränger in i burken och då hur länge den ska kokas och även hur stor burk du kan använda. Större burk brukar ofta betyda längre tid.

Ingredienser är som sagt ett problem. Mjölkprodukter ska tydligen vara ett kontroversiellt ämne och anses inte säkert som det är nu. Detsamma gäller spannmål. Jag gissar att du ofta använder mjölk eller spannmål i din matlagning, eller hur?

Sen är det såklart grejen med vad som blir bra att konservera. Färdiga recept är ju såklart framtagna för att konserveras och kommer såklart garanterat resultera i så bra produkt som möjligt. Ofta tar man hänsyn i recept till att maten faktiskt tillagas under själva konserveringsprocessen.

## Headspace/luftspalt, tid och så vidare. Recept!

I poddavsnittet så pratas väldigt lite om hur mycket burken ska fyllas i olika situationer. Det enkla svaret är: Det står i receptet. Ska du nu göra något eget, ja men kolla då något snarlikt recept och kör lika som det i så fall. Detsamma gäller inkokningstiden. Olika saker behöver konserveras olika länge. Kolla på [www.healthycanning.com](http://www.healthycanning.com) och du kommer hitta hur många recept som helst. Jag rekommenderar dig verkligen att följa recept i första hand istället för att improvisera. De är testade och du *vet* att det blir rätt. Och gott. Många recept tillåter dig dessutom att leka lite. Dessutom spelar t.ex. kryddor ingen roll alls för säkerheten, utan det är bara att köra som du vill om du vill ändra något där. Tanken med specifik luftspalt är i alla fall att maximera mängden i burken utan att innehållet börjar spruta ut ur burken under konserveringsprocessen (det expanderar ju såklart när det hettas upp). Dessutom får man ett sämre vakuum med för lite innehåll i burken.

## Vattenbadskonservering

Jag saknade lite mer information om vattenbadskonservering, men det förklaras väl med att Kallen inte har testat det själv. Det omnämns, men bara lite. Tryckkonservering är nämligen bara ett "nödvändigt ont" när du ska konservera grönsaker, kött och fisk. Ska du göra sura saker som [inlagd gurka och picklad rödlök](https://www.healthycanning.com/pickles), [sylt](https://www.healthycanning.com/jam), [frukt](https://www.healthycanning.com/fruit) eller varför inte [salsasås](https://www.healthycanning.com/salsa) (rekommenderas!!) så kommer du bara försämra produkten om du tryckkonserverar. Dessutom måste du som bekant ha en tryckkonserveringsapparat. Nä, kör istället på gammal hedelig vattenbadskonservering i vanlig kastrull i dessa fall. Det blir superbra och du behöver verkligen inte oroa dig. Ska skriva ett inlägg om hur man gör någon gång.

## Slutsats

Det här var väl allt jag hade för den här gången. Tycker trots allt att det är ett kanonavsnitt som du bör lyssna på, men att det fanns några saker man kan diskutera. Likt Patrick så fastnar jag lite lätt i detaljer och börjar glida ifrån ämnet ibland, så ursäkta att det blev lite långt. ;)
