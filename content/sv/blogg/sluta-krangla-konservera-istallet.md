---
title: "Sluta krångla! Konservera sylten i vattenbad istället."
date: 2022-07-30 08:25:00 +0100
categories:
 - konservering
tags:
 - konservering
 - vattenbadskonservering
 - sylt
 - självhushållning
---
Läs valfritt svenskt syltrecept på internet och du kommer läsa saker som "rengör burkarna NOGA!", "förvara svalt", "natriumbensoat", "vänd burkarna upp-och-ner" och så vidare och så vidare. Poängen är att det är en himla massa krångel för att inte din sylt ska bli dålig innan du hinner äta den. Men lugn. Det finns ett enklare sätt.

Om du stoppar ner dina syltburkar i ett vattenbad och kokar dem i tio ynka minuter så slipper du verkligen allt sånt krångel. Du behöver inte rengöra burkarna med salpetersyra från yttre rymden för att sedan behöva kirurgiska sterila handskar så fort du ska ta i dem, du behöver inte ha i något konserveringsmedel (nej, inte ens socker!), du behöver inte vända burkarna upp-och-ner och, hör och häpna, du kan förvara dem i rumstemperatur!

Hur gör man då? Jag har i stort sett sagt det redan. Men vi tar den lite längre varianten också.

## Vattenbadskonservering av sylt

Du kan ta i stort sett vilket syltrecept du vill egentligen, så länge det bara innehåller nordiska frukter eller bär (samtliga har lågt pH nog för att inte kunna ge dig botulinum) och "kryddor" såsom socker, salt eller annan smaksättning och inget annat. Äppelmos kan med fördel ha i en liten skvätt citronjuice, men i övrigt är det bara att köra. När det gäller socker, som vi ju ofta har i sylt, så har de ingen egentlig konserveringseffekt under själva lagringen utan först när burken öppnats. Så vill du göra något utan socker så går det jättebra, men då behöver du också äta upp burken snabbare. Dessutom måste du såklart hitta ett sätt att få det till att faktiskt bli en sylt i konsistensen och inte "bär i en burk".

Så hör gör du:

### Steg-för-steg

- Koka upp din sylt.
- Ta en slev och häll i den i dina lagomt rengjorda burkar som gärna kan vara lite varma för att inte riskera att spricka. Skaffa med fördel en burktratt så går det enkelt. 
- Lämna 1 cm luftgap mellan sylten upp till locket. 
- Stoppa ner en kniv i varje burk och ploppa ut eventuella luftbubblor. 
- Ta en blöt trasa och torka bort eventuellt kladd från burkarnas kant. 
- Stäng locken allt eftersom du blir klar med varje burk.
- Stoppa ner burkarna i din konserveringskastrull (som har en bottenplatta eller kökshandduk i botten, annars kommer de hoppa runt och riskera att gå sönder) där vattnet kokar.
- Se till att vattnet täcker burkarna helt.
- Koka i 10 minuter och låt inte kokningen stanna under dessa 10 minuter.
- Ta upp burkarna och låt dem stå på diskbänken i ca 12 timmar tills de har kallnat.

Du kommer nu ha ett vakuum bortom denna värld och en sylt som kan stå i stort sett för alltid i rumstemperatur. Så sjukt nice. Varför har jag krånglat så mycket förut!? tänker en och annan just nu.

Lycka till!
