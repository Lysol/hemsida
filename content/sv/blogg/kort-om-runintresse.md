---
title: "Kort om mitt runintresse"
date: 2022-01-23T19:23:56+01:00
draft: false
toc: false
images:
categories:
 - runor
tags:
 - runor
---
För att man ska få någon slags känsla för vad jag eventuellt kommer att skriva om här så passar jag på att skriva lite om mitt runintresse lite kort. Väldigt kort.

Jag snöade in på det här med runor, alltså det gamla skriftspråket, för kanske åtta-tio år sedan. I grund och botten är jag lärd av Wikipedia, men 2019-2020 läste jag en distanskurs på Uppsala universitet i ämnet runologi. Det var en enkel grundkurs, men det var en väldigt rolig kurs att läsa.

En poänglös men i min mening ganska rolig fundering jag haft i många år är hur det hade sett ut om Sverige hade fortsatt använda runor som skriftspråk ända till idag. Hade det ens varit möjligt? Jag är verkligen långt ifrån kvalificerad att svara på det här, men det är ändå kul att amatörspekulera kring.

Om jag får tummen ur så kan det hända att jag någon gång publicerar en runfont som täcker in de bokstäver vi använder idag, men som runor istället för latinska tecken. Den grundläggande vikingatida runraden hade bara 16 runor, vilket gjorde den väldigt klumpig att både läsa och skriva med. I runkonstens slutskede runt 1300-1400-talet tog därför runmästarna ganska mycket inspiration från det fiffiga latinska alfabetet som har så många användbara tecken. Man fyllde helt enkelt på med fler runor för fler ljudvärden. Om man därför idag bara kompletterar med någon enstaka sista runa så har vi ett helt alfabet som känns väldigt naturligt att skriva modern svenska med. Jättekul, tycker jag av någon jättekonstig anledning.

Det kommer mera, som ett TV-program sa på den gamla goda tiden.
