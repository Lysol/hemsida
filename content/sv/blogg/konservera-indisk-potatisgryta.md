---
title: "Konservering av indisk potatisgryta"
date: 2022-04-27 08:15:00 +0200
categories:
 - konservering
tags:
 - konservering
 - tryckkonservering
 - pressure canning
 - soppa
 - självhushållning
 - mat
---
Dags för ny soppkonservering!

Även denna gång kände jag för att göra ett recept från undertian. Receptet heter [Kryddig potatisgryta](https://undertian.com/recept/kryddig-potatisgryta-med-naan/). Det lite luriga här är att det ju är en gryta och inte en soppa, och receptet från [USDA](https://nchfp.uga.edu/how/can_04/soups.html) (50% fast, 50% flytande) gäller ju soppor (receptgrunden finns i mer utförlig version [på healthycanning.com](https://www.healthycanning.com/usdas-your-choice-soup-recipe)). Därför kommer det här receptet bli en aning mer flytande än det är om du lagar maten enligt ordinarie recept på undertian.

En annan grej som skiljer sig är att en timme och femton minuters tryckkonservering totalt tillintetgjorde äppelbitarna i burkarna. Finns inte spår av äppelbitar kvar, haha. Men de ger ju smak så de tillför väl lite trots allt. :)

Denna batch gav ca **10 st burkar på 1 liter**. Lite *drygt* räknat, man kan egentligen med fördel köra 11 burkar för det blev lite lite väl fullt i burkarna med 10, men min konserveringsapparat rymmer bara 5 burkar på 1 liter + 5 burkar på 500 ml ovanpå, och jag hade inga 500 ml burkar kvar just nu.

Min anpassning ser då ut som följande:

## Recept på kryddig indisk potatissoppa

Dagen före ska du blötlägga:

- 6 dl torkade vita bönor

Blötlägg dessa dagen innan i mellan 12-24 timmar. När bönorna har legat i blöt i minst 12 timmar kan du sätta igång med följande:

- 9 vitlöksklyftor, finhackade
- 3 st röd chili (jag smulade ner gammal torkad chili som jag hade hemma)
- 3 tsk kanel
- 3 msk gul curry
- 9 msk riven ingefära (jag körde en hel mindre ingefära bara)

Samt följande:

- 4 pkt passerade tomater eller kort och gott cirka 1,2 kg tomater som är passerade.
- 3 liter vatten
- 4 st grönsaksbuljongtärningar
- salt efter smak

Ovanstående ska alltså bli "buljong", dvs den flytande delen i soppan. Fräs vitlök, chili, kanel, curry och ingefära och häll sedan i passerade tomater, vatten, buljongtärningar och salt efter smak. Medan detta kokar upp kan du hacka upp:

- 1300 g potatis
- 650 g äpplen
- 300 g gula lökar, grovt hackade som klyftor
- De färdigkokta vita bönorna (det blev 1015 g för mig)

Fördela det fasta på runt 10-12 (förvärmda) burkar på 1 liter vardera. Det bör då bli ungefär halva burken med 3 cm luftspalt längst upp medräknat.

Häll sedan i den flytande tomatvätskan i varje burk så att du täcker upp till 3 cm under burkens kant. Det är alltså det som kallas "headspace", en luftspalt helt enkelt. Den måste vara där av två anledningar. Om du har för *lite* headspace kan du få för dåligt vakuum. Om du har för *mycket* headspace så kommer maten pressas ur burken under konserveringsprocessen.

För varje burk du fyller ställer du ner den i konserveringsapparaten så att burken inte hinner kallna. Fyll så många burkar som du får plats med i din apparat till att börja med. Stäng locket på apparaten och **konservera under tryck i 1 timme och 15 minuter, alltså 75 minuter**. Om du bara har burkar på 500 ml så räcker dock 60 minuter. Följ min [guide]({{< ref "/blogg/tryckkonservering.md" >}}) för tryckkonservering om du inte kommer ihåg hur man gör.

Jag ska uppdatera med lite bilder så småning om! Grytan/soppan blev väldigt väldigt bra i alla fall och blev nog godast hittills av det jag konserverat i "färdig mat"-variant.

Ha de!
