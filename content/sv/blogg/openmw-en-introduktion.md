---
title:  "En introduktion till OpenMW"
date:   2021-01-31 16:00:00 +0100
tags:
 - openmw
categories:
 - openmw
---
Sådär, dags att testa göra något random blogginlägg om något jag gillar.

Jag är engagerad i ett projekt som heter [OpenMW](http://www.openmw.org). Om du är intresserad av datorspel/rollspel/öppen källkod eller liknande så kan jag rekommendera en titt.

OpenMW är helt enkelt en helt ny spelmotor gjord för att köra 2002-års Elder Scrolls-rollspel "Morrowind". Spelmotorn är licensierad i GPL och har alltså helt öppen källkod. OpenMW använder bland annat biblioteken OpenSceneGraph (grafik), Bullet (fysik), myGUI (grafiskt gränssnitt) och recastnavigation (navigeringsmotor för AI). Morrowind är ett av mina favoritspel genom tiderna, och då jag även är intresserad av modding och fri mjukvara blev jag helt fast när jag stötte på det här projektet kring 2012.

Spelmotorn har som första mål (1.0) att kunna replikera Morrowinds spelmotor NetImmerse, utan buggar. Projektet är väldigt nära detta mål nu, och tanken är att när detta mål är uppnått ska hela motorn öppnas upp för att kunna erbjuda oändliga modifikationsmöjligheter. Redan nu finns en multiplayer-fork (arbetsnamnet är tes3mp. Testa, för det fungerar hur bra som helst redan nu) som inom en snar framtid är tänkt att sammanfogas med moderprojektet.

I communityn går jag under namnet "Lysol" och är engagerad i PR och liknande. Hoppas vi ses där!

Kan hända att jag bloggar mer om det projektet framöver.
