---
title: "Konservering med tryckkonserveringsapparat"
date: 2022-02-15 13:45:00 +0100
categories:
 - konservering
tags:
 - konservering
 - tryckkonservering
 - pressure canning
 - självhushållning
 - mat
---
Det finns en hel del information att hitta om så kallad tryckkonservering, det man på andra sidan atlanten kallar *pressure canning*. Problemet är att allt är på engelska och anpassat för en amerikansk/kanadensisk publik. Därför tänkte jag här göra en svensk guide, baserad på [Healthy Cannings steg-för-steg guide till tryckkonservering](https://www.healthycanning.com/pressure-canning-step-by-step/). Guiden är helt fenomenal och lätt att följa. Den är också väldigt noggrann och har en hel drös med källhänvisningar och citat som gör den ganska klumpig att läsa om man bara snabbt vill ha en guide att följa när man konserverar. Därför kommer jag nu rekommendera alla att läsa igenom den först för att sen, för den som vill, använda min lite nedbantade översättning i praktiken.

Innan vi börjar vill jag bara påpeka att långt ifrån allt behöver tryckkonserveras. Kortfattat så kan man vattenbadskonservera (dvs, koka burkar helt dränkta i vatten) allt som har lågt pH. Detta inkluderar saker som frukt och bär (sylt, mos, osv) och inläggningar (smörgåsgurka, inlagda rödbetor, picklad rödlök, osv). Följ dock alltid ett recept som är testat (se exemplevis recepten på [Healthy Canning](https://www.healthycanning.com/)) om du vill vara helt säker på att du inte utsätter dig själv och andra för livsfara, dvs botulism. Det som *ska* tryckkonserveras är allt med högt pH, vilket i praktiken blir grönsaker, rotfrukter, fisk och skaldjur samt kött. Även detta ska tillagas efter testade recept om man vill vara säker på att inte förgifta sig själv.

Då börjar vi!

## Vad behöver jag?

- Tryckkonserveringsapparat (måste tyvärr importeras från USA eller Kanada, eBay eller Amazon är bra tips). Vanliga märken är Presto, All American och Mirro. Jag har en Presto 23 Qt och är väldigt nöjd. Se till att skaffa en modell som klarar induktion om du har eller planerar att skaffa en induktionshäll.
- Med fördel bör du ha en "weighted pressure regulator". Mer om den senare, men det är en stark rekommendation
- En tång att lyfta burkar med
- En tratt som helst är kompatibel med burkarna
- Burkar som funkar att konservera med. Ska man strikt följa amerikanarna så får det absolut inte vara skruvlock. Jag rekommenderar Weck-burkar. Amerikanska Ball-burkar är på tok för dyra att importera för att jag ska rekommendera dem.
- Kastrull, slev och sånt där som du ändå redan har hemma

## 1. Kolla vilket tryck receptet kräver

Innan du börjar följa ett recept, kolla vilket tryck som krävs. Anpassa sedan trycket för just där du befinner dig, då du kommer behöva högre tryck om du befinner dig på hög höjd. Trycket är nämligen lägre på högre höjd och du kommer alltså behöva lägga på mer tryck i konserveringsapparaten för att uppnå samma temperatur i den.

[Här](https://www.healthycanning.com/pressure-canner-altitude-adjustments/) finns en tabell där du kan se vad som gäller där du bor. Kolla en höjdkarta om du inte vet vilken höjd du bor på.

## 2. Kolla inkokningstiden ("processing time") som receptet kräver

Se till att du inte slarvar med den tiden som receptet kräver att du kokar burkarna i. Det kommer även ta lite tid efteråt för kastrullen att kallna innan du kan ta ut burkarna, så se till att du verkligen har en ledig dag när du ska göra det här.

## 3. Fyll apparaten med rätt mängd vatten

En modern Presto 16/23 Qt ska ha **tre liter** vatten. Andra modeller kan behöva andra mängder. Det står i instruktionsboken hur mycket just din ska ha. Tips är att ha ett par matskedar vinäger i vattnet så kommer burkarna och apparaten vara mycket renare när du tar ut dem.

Så för att klargöra lite: Du ska alltså *inte* dränka burkarna med vatten när du tryckkonserverar, till skillnad från när du vattenbadskonserverar.

## 4. Ställ ner burkarna

1. Ställ burkarna upprätt. Lägg absolut inte dem åt sidan, för då riskerar innehållet att pressas ut istället för att luften pressas ut.
2. Vattnet ska inte koka när du ställer ner burkarna.
3. Om innehållet i burkarna är varmt, ha runt 80 grader varmt i konserveringsapparaten
4. Om innehållet i burkarna är kallt, ha någonstans runt 60 grader

Temperaturen på vattnet i konserveringsapparaten har egentligen bara att göra med att minimera risken för att burkarna spricker när du stoppar ner dem i badet.

## 5. Sätt på locket och låt det pysa i 10 min

1. Sätt på och lås fast locket
2. Börja utan att ha vikten/tryckregulatorvikten på ventilationshålet först
3. Om du inte redan har full värme på spisen så är det läge nu
4. Ganska snart kommer det att börja koka för fullt där inne. Till slut kommer det börja pysa ur ventilationshålet.
5. När du hör, känner och ser att det kraftigt pyser ut ånga så sätter du en timer på tio minuter

## 6. Lägg på vikt för att öka trycket

När du låtit apparaten pysa i tio minuter så lägger du på vikten på ventilationshålet. Trycket kommer då att börja stiga när du tittar på manometern.

Kom ihåg att om du har en tryckreglerande vikt så måste du se till att ställa in den på 10 eller 15 pounds beroende på hur högt ovanför havsnivån du bor. Kolla länken i steg ett igen om du har glömt.

När du börjar närma dig det tryck du ska upp till så kan du sänka värmen en aning så att du får lite mer kontroll när du nått rätt tryck.

## 7. Starta timer när du nått rätt tryck!

Om du har en tryckreglerande vikt så kommer den börja gunga när du nått rätt tryck. Se till att du hittar rätt värme för att hålla just detta tryck under hela tiden. Jag med vår gamla keramikhäll vrider från 12 till 4-5 för perfekt temperatur på plattan.

När du nått rätt tryck är det dags: starta din timer nu.

## 8. Håll trycket: Varken mer eller mindre

Under hela proceduren, se till att hålla precis rätt tryck. Tryckregulatorvikterna beter sig lite olika för de olika märkena. Min Presto gungar konstant vid rätt tryck, medan till exempel en All American ska gunga 1-4 gångar per minut. Tydligen måste man börja om från början med sin timer om man tappat trycket, så se verkligen till att du inte gör det.

## 9. Tiden är ute!

När inkokningstiden har gått klart kan du stänga av plattan. Om du kan så får du flytta apparaten bort från den heta plattan, men gör ingenting annat. Låt även vikten ligga kvar. Det är ett högt tryck i apparaten just nu och rent livsfarligt att öppna. En modern apparat tar cirka 20 minuter att tappa trycket från och med nu. Du får absolut inte påverka den så att den kallnar fortare, den måste kallna i rumstemperatur.

## 10. Vänta tills trycket är nere på noll

Manometern säger såklart 0, men det kan finnas lite kvar ändå. Min Presto har en liten plugg i apparatens lock som sjunker ihop när trycket är nere på noll.

När trycket är nere på noll tar du bort vikten. Vänta sedan tio minuter innan du öppnar locket!

## 11. Ta bort locket

Har du väntat tio minuter efter att du tog bort vikten? Bra. Det är svinvarmt inne i apparaten, så ha grytvantar och akta ansiktet när du öppnar.

## 12. Plocka ur burkarna

Plocka ur burkarna ur apparaten och ställ dem på en bänk eller något sånt. Ställ dem *inte* vid ett öppet fönster, de måste få kallna långsamt i vanlig rumstemperatur.

## 13. Låt burkarna kallna i 12-24 timmar

När burkarna kallnat i 12-24 timmar så kan du ta bort klämmorna, skruvringarna eller vad det nu är som gäller på just din burk. Locket ska sitta fast av sig självt och du ska kunna lyfta burken i locket utan att burken öppnas om du vill vara säker på att du har fått vakuum.

Vill du torka av burkarna så gör det. Tips är såklart också att märka dem så du vet när du gjorde dem.

Ställ dem i ett skafferi eller något sånt.

## 14. Klart!

Grattis!
