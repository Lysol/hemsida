---
title: "LineageOS som 'dumtelefon' istället för knapptelefon"
date: 2022-02-06T18:50:04+01:00
draft: true
toc: false
images:
categories:
 - lineageos
tags:
 - lineageos
 - smartphone
 - dumtelefon
 - knapptelefon
---
En udda detalj med mig som i stort sett alla förr eller sedan kommer reagera på är mitt val av telefon. När smartphone-eran började på allvar och Android började komma som konkurrent till Iphone skaffade jag en HTC Legend. Det lär ha varit runt 2010. Sjukt bra telefon. Plötsligt försvann min laddare (folk hade liksom inte tusen micro-USB laddare hemma då) och jag fick plocka fram en gammal nokialur. Vilken frihet! Och jag kom plötsligt ihåg hur najs det var att skriva sms samtidigt som jag promenerade till bussen -- *utan* att titta på displayen samtidigt! Dessutom så var den inte alls så svår att motstå som min HTC. Istället för att köpa ny laddare så sålde jag min HTC och var sedan dess utan smartphone.

De senaste 7-8 åren har jag haft en Nokia 301. Den perfekta knapptelefonen. Ärligt talat, den här helt klockren och jag är genuint ledsen att knapptelefoner inte fortsatte utvecklas. Nokia hade verkligen tagit knapptelefoner till perfektion med sitt Symbian-system, och att skriva med T9 är verkligen inte så illa som folk minns det.

Men då till frågan: Hur sjutton gjorde du med Swish? BankID? Kolla bankkontot när du är ute? Postnordappen?! Svaret är surfplatta. Det var alltså inte riktigt så radikalt som det först kanske lät. Men sanningen är såklart att BankID och Swish i stort sett inte går att klara sig utan idag. BankID finns till dator, jag vet, men jag kör Linux och där finns det inte. Jag och min fru delade därför på surfplatta för att ingen av oss ska fastna för mycket i frestelsen. Jag har inte haft den med mig till jobbet och så vidare.

Det här har funkat jättebra, men i ärlighetens namn så har jag ändå önskat att jag kunnat ha BankID och andra nödvändiga appar i min Nokia 301. Här kommer vi till vad som hände för några dagar sedan: Jag skaffade en smartphone! Men istället för att köra Android så installerade jag [LineageOS](https://www.lineageos.org) tillsammans med microG i pico-utförande, vilket alltså är "bare-minimum" google för att de flesta appar ska fungera. På telefonen har jag alltså:

- Diverse bankappar som Sparbanken och Avanza
- Swish
- BankID, Postnord
- GPS i form av OsmAnd+ (alternativ till google maps)
- Metronom (jag är ju trummis)
- Stämapparat (jag spelar även gitarr)
- Podcastapp (tidigare har jag laddat ner poddar och fört över manuellt till min telefons minneskort. Jag lyssnar mycket när jag t.ex. bygger och sådär)
- SMHI
- SVT Nyheter (kan väl sträcka mig så långt utan att bli beroende)
- Wikipedia. Man måste kunna gubbgoogla ibland.

Det här är i stort sett alla appar jag har. Ja, alltså på riktigt, jag har inte ens en webbläsare. Jag tror jag har hittat ett slags kompromiss här mellan smart telefon och dum telefon. En telefon som är smart, men som inte har det där som verkligen drar i ens uppmärksamhet konstant.

Så vad har vi för fördelar jämfört med tidigare?

- Jag behöver inte vänta tills jag kommer hem eller ringa min fru varje gång jag behöver använda BankID och jag inte är hemma
- Jag kan kolla bankkontot och själv flytta mellan konton ifall jag ska
