---
title:  "Sajten ligger nu på GitLab"
date:   2021-07-18 22:04:00 +0100
categories:
 - webb
---
För den som är intresserad (??) så genererar jag inte längre sajten lokalt på min dator för att sedan ladda upp den via sftp till min server. Nej, nu är jag så pass sofistikerad att jag har hemsidan på [GitLab](http://www.gitlab.com/Lysol/hemsida). Om du tycker jag skriver något helt knäppt här så kan du till och med skicka en *Merge Request* till mig med förslag på uppdatering! Varför någon skulle vilja göra det på en annan persons personliga hemsida...

Hur som helst. Det är lite kul att testa sig fram med det här. Det är smidigt som attan, men utan att för den delen bli så tungt och segt som exempelvis wordpress kan bli. Ska väl försöka göra något slags inlägg framöver där jag beskriver hur jag gått till väga. Koden kan du iofs se på gitlab också.

Hörs!
