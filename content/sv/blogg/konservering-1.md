---
title: "Konservering!"
date: 2021-09-06 08:45:00 +0100
categories:
 - konservering
tags:
 - konservering
 - självhushållning
 - mat
---
Nu är hösten här. För oss har detta inneburit flera långa dagar med att konservera mat – främst inläggningar i vinägerlag – genom metoden "hermetisk inkokning" för att slippa ha maten förvarad kallt. Jag skulle på sikt vilja göra en hel sida här dedikerad till just konservering genom inkokning, eftersom det är en konst som gått  förlorad i Sverige. I USA görs detta väldigt mycket fortfarande på landsbygden (kallas *canning*), och det finns en hel del att hämta därifrån eftersom konsten där har fortsatt att utvecklas sedan den tiden vi fortfarande höll på med det här. Den informationen som kommit därifrån skulle jag gärna göra tillgänglig och lättåtkomlig även här, på svenska. Det man behöver lära sig handlar om säkerhet och hälsa, eftersom det såklart krävs ett och annat för att kunna äta något som stått i ett helt år i rumstemperatur utan att riskera sin hälsa.

Det här blogginlägget kommer inte behandla ämnet mer än att jag tänkte ge en liten inblick i vad det handlar om.

Så vad är då hermetisk inkokning? Hermetisk inkokning innebär att du lägger mat som du vill konservera i en burk med lock. Denna burken läggs sedan ner i kokande vatten under en specificerad tid för att pressa ur luften ur burken när innehållet expanderar. När innehållet sedan kallnar efter att burken kokat klart kommer det skapas ett vakuum som håller locket på plats. Innehållet ska nu, förutsatt att du vet vad du håller på med, vara säkert att förvara i rumstemperatur: Burken har blivit en helkonserv.

Är det då bara att tuta och köra nu? Koka sina burkar i slumpvis tid? Nej verkligen inte. Det kan faktiskt gå *riktigt* snett om du inte vet vad du håller på med. Det största (men inte enda!) problemet stavas [clostridium botulinum](https://www.livsmedelsverket.se/livsmedel-och-innehall/bakterier-virus-parasiter-och-mogelsvampar1/bakterier/clostridium-botulinum), en bakterie som trivs väldigt bra i syrefria miljöer, det vill säga precis den miljö vi skapar när vi gör en helkonserv. Mer specifikt är det ett toxin som bakterien producerar som är problemet. Det råkar nämligen vara det mest potenta toxinet i världen och är alltså totalt livsfarligt. Men borde inte bakterien dö när du kokar innehållet? Ja, absolut! Men problemet är att denna bakterie är sporbildande; den lägger ägg, kan man säga. Dessa sporer dör först vid 121 grader, och vatten kokar som säkert bekant redan vid 100 grader. Det blir aldrig varmare än så. Inte ens om du stoppar burken i en ugn på 275 grader blir vattnet varmare än 100 grader.

Det finns några lösningar på detta, om du vill kunna förvara dina burkar i rumstemperatur:

- Göra inläggningar med ättikslag eller vinägerlag skapar en sur miljö (låg pH) som gör att botulinumgiftet inte kan produceras. Detta kräver i sin tur oftast en hel del socker för att det ska vara gott att äta. Ibland vill du ha det här såklart eftersom inlagda grönsaker är sjukt gott, men ibland vill man ju bara ha vanlig mat.
- Koka innehållet i burken efter att du öppnat den, så dödar du det potentiella giftet. Innehållet blir också mycket tråkigare att äta eftersom du kokar det en gång till. Ärligt talat känns det också lite läskigt tycker jag.
- Göra inkokningen med en *pressure canner*, en tryckkonserveringsapparat. Dessa ökar trycket under kokningen, vilket leder till en ökad koktemperatur för att uppnå den magiska gränsen på 121 grader. Då dör även sporerna till bakterien och burken blir således helt steril, åtminstone från alla bakterier som vi kan ta skada av.

I USA är punkt två helt utesluten, men det görs fortfarande på många håll i Europa. Jag tycker att man bör investera i en riktig pressure canner av märken som Presto, Mirro eller All American om man vill kunna göra annat än bara sötsura inläggningar – för smakens skull såklart, men framför allt för sin egen säkerhets skull.

Det finns så otroligt mycket att prata om, allt från konserveringsapparater och recept till burkar! Vi får se om jag får tid att realisera min vision om en hel sida dedikerad till det här. Ska få med så mycket bilder jag kan då! :)

Tack för att du läste. Over and out!
