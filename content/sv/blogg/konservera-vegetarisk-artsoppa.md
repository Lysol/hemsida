---
title: "Konservering av vegetarisk ärtsoppa"
date: 2022-03-26 15:15:00 +0100
categories:
 - konservering
tags:
 - konservering
 - tryckkonservering
 - pressure canning
 - soppa
 - självhushållning
 - mat
---
Efter succén (?) med den veganska gulaschen ville jag testa på något mer. Ärtsoppa är gott, men den enda konserverade ärtsoppan som finns som också är god är Soldatens (icke-vegetariska) ärtsoppa. Det finns en vegetarisk, men den är helt enkelt inte särskilt god. Eftersom hustrun är vegetarian och vi brukar äta ärtsoppa på tub lite nu och då så fick det bli ärtsoppa. Hur gör man detta då?

![Ärtsoppa](/bilder/konservering/tryckkonservering/artsoppa/01.jpg)

## Kombinera ihop två recept

Man kombinerar ihop ett recept från [receptfavoriter.se](https://receptfavoriter.se/recept/vegetarisk-artsoppa) med ett testat och säkert recept från amerikanska Ball och kanadensiska Bernardin, publicerat på [healthycanning.com](https://www.healthycanning.com/home-canned-pea-soup). Jänkarna (och kanadensarna verkar det som) är ju lite knasiga, så de puréar ärtorna. Skandal!

Enligt receptet så kan man skippa köttet och citronjuicen, så vi börjar där. Allmänt för tryckkonservering gäller också att kryddning går att byta ut lite hur man vill, så det som återstår som krav är då vatten, ärtor, morötter och lök.

I detta recept gäller egentligen "split peas", vilket alltså är skalade ärtor. Vafalls. Nån jänkaridé som jag struntar i. Skillnaden blir då att vi måste blötlägga ärtorna, vilket inte behövs när man har skalade ärtor.

Kryddorna körde jag helt efter receptet på receptfavoriter.

Till sist så tredubblerade jag receptet eftersom jag ville ha mycket soppa.

Själva principen med att tryckkonservera (för du bör inte göra detta på annat sätt) har jag gått i genom i ett tidigare [blogginlägg]({{< ref "/blogg/tryckkonservering.md" >}}).

## Ingredienser

- 6 liter vatten (det blev rätt mycket, så det kan gå att ta lite mindre tror jag men häng mig inte om du dör av botulism)
- 1500 g blötlagda ärtor (vägda när de är torra)
- 9 skalade och hackade mellanstora morötter
- 3 hackade halvstora lökar
- 3 lagerblad (frivilligt)
- Salt enligt smak. Dvs mycket.
- 6 grönsaksbuljongtärningar
- 1,5 tsk torkad timjan
- 1,5 tsk torkad mejram

Detta blev runt 5 burkar på 1 liter samt 5 burkar på 0,5 liter + lite extra att äta direkt.

## Instruktioner

1. Koka upp de blötlagda ärtorna och låt sjuda tills de är mjuka (knappt en timme)
2. Häll i resterande: morötter, lök, kryddor och nedsmulade grönsaksbuljongtärningar och låt sjuda i ytterligare ca 30 minuter
3. Smaka av med salt. Det behövs mycket.
4. Plocka ur lagerbladen
5. Häll i burkar
6. Lämna 3 cm "headspace", alltså avstånd till locket
7. Torka av burkkanten, sätt på lock och ställ ner i tryckkonserveringsapparaten
8. För 99% av alla i Sverige så ska man köra med 10 lbs i tryck, men om du är en av de få som bor över 300 m.ö.h. så ska du köra med 15 lbs i tryck.
9. Har du bara 0,5 L burkar så kör du i 75 minuter, men om du som jag har en eller fler burkar på 1 L så måste du köra i 90 minuter (även om du har mindre burkar också)

## Slutsats

Det här var ett kul recept som jag rekommenderar. Inga problem att ha med kött om du vill ha det, i så fall blir det ca 600 gram du ska ha. Häll i det i steg 2.

Soppan kommer verka otroligt vattning när du häller i den i burkarna, men den kommer tjocka till sig rejält. Den kommer till och med skikta sig en aning i burken när den kallnat så att du får en tjock ärtklump i botten och nästan bara vatten längst upp. När du öppnat en burk, värmer upp den och rör om så kommer den bli perfekt.
