---
title: "Konservering av vegansk gulasch"
date: 2022-03-06 12:15:00 +0100
categories:
 - konservering
tags:
 - konservering
 - tryckkonservering
 - pressure canning
 - soppa
 - självhushållning
 - mat
---
Igår gjorde jag min första konserverade färdiga maträtt. Soppor är ganska lätta att konservera som färdiga rätter, så det fick bli en gulasch som jag baserade på ett recept från [under tian](https://undertian.com/recept/vegansk-gulasch/). Den här soppan har jag gjort många gånger här hemma eftersom den är asgod, så när jag ville testa att konservera en färdig maträtt så kändes den given.

Hur funkar det då, är det bara att göra valfri soppa och slänga ner i burkar och konservera? Nja. Men nästan. USDA (US Department of Agriculture) har ett "ramverk" för konserveringsbara soppor som innebär att du kan göra näst intill oändliga kombinationer med soppa, bara du följer några få riktlinjer.

Riktlinjerna finns beskrivna i detalj på [healthy canning](https://www.healthycanning.com/usdas-your-choice-soup-recipe) och jag rekommenderar att du läser igenom på den sidan noga om du vill följa receptet. Jag rekommenderar också att du redan sen innan testat på att tryckkonservera någon råvara, typ bönor, innan du testar på det här, bara så att du har fått känna på hur själva [tryckkonserveringsmetoden]({{< ref "/blogg/tryckkonservering.md" >}}) går till. Principen är i alla fall att du ska ha 50% solida innehåll och sedan fylla upp resterande 50% av burken (med 3 cm headspace inräknat) med vätska. Det finns också en liten lista över saker som du inte kan ha med i din soppa. Återigen, läs noga på healthy canning.

## Nu kör vi!

Jag dubblerade receptet från *under tian* till 8 portioner. Sen skalade jag och hackade upp alla rotfrukter och grönsaker.

![upphackade grönsaker och rotfrukter](/bilder/konservering/tryckkonservering/gulasch/01.jpg)

Min första tanke var att jag skulle fördela upp alla ingredienser jämnt i alla burkar så att det blev lika mycket i alla, men jag insåg snabbt att det var krångligt att få till eftersom allt ska vara upphettat för att inte burken ska riskera att spricka. Därför blev det lite konstigt den här gången att jag blandade ihop alla ingredienserna i en kastrull som jag kokade upp. Sen fördelade jag upp alla rotfrukter och grönsaker så gott jag kunde i burkarna.

En annan gång kanske jag kokar upp vatten som jag häller i skålarna istället, det vore smidigt om man kunde enkelt fördela lika mycket i alla burkar...

Till sist tog jag belugalinserna (som jag förvällt en halvtimme genom att lägga dem i vatten som jag kokat upp men sen direkt tagit bort från plattan) och fyllde så att ca 50% av burken är fylld.

![rotfrukter och grönsaker](/bilder/konservering/tryckkonservering/gulasch/02.jpg)

Efter detta så fyllde jag på med "buljongen", i mitt fall vatten med lite passerade tomater, rökt paprikapulver, mald torkad chili och några buljongtärningar i, så att det bara var ca 3 cm kvar till burkens kant.

![fylld burk](/bilder/konservering/tryckkonservering/gulasch/03.jpg)

Till sist ska man såklart konservera. Burkarna stoppades ner i tryckkonserveringsapparaten, jag stängde locket och fick upp trycket och konserverade i 1 timme och 15 minuter.

Färdigt! Så här såg burkarna ut när processen var klar.

![färdiga burkar](/bilder/konservering/tryckkonservering/gulasch/04.jpg)

Ursäkta halvkassa bilder för övrigt. Det är svårt att ta bilder i motljus precis vid ett fönster, haha. Jag tog en sista bild i fotostudio när allt kallnat, så den får kompensera. Behold!

![burk!](/bilder/konservering/tryckkonservering/gulasch/05.jpg)

Tack för att du läser! Vi ses.
