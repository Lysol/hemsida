---
title:  "OpenMW har nu Lua som skriptsystem"
date:   2021-07-21 08:30:00 +0100
categories:
 - openmw
tags:
 - openmw
---
OpenMW är, som jag tidigare [pratat om]({{< ref "/blogg/openmw-en-introduktion.md" >}}) en ny spelmotor med öppen källkod som läser spelfilerna från det gamla rollspelet Morrowind.

Jag tänkte bara ge en liten uppdatering. Vi väntar fortfarande på att en *release candidate* ska klara all testning felfritt så att nästa version av OpenMW, 0.47.0, ska kunna släppas som en officiell release. Därför är det lite sparsamt med nyheter på den [officiella hemsidan](https://www.openmw.org) just nu, eftersom man inte vill att saftiga nyheter ska överskugga det faktum att det när som helst släpps en ny release.

Men jag tänkte komma med en liten nyhet här på min privata webbsida istället: OpenMW har för några veckor sedan implementerat grunderna till det nya skriptsystemet Lua i spelmotorn. Morrowinds gamla skriptsystem, *mwscript*, är väldigt begränsat och ganska drygt att koda i. Därför kommer OpenMW i framtiden att använda Lua som skriptsystem istället. Skript från spelet Morrowind och tillhörande moddar kommer att i bakgrunden översättas till Lua när implementeringen är färdig. Just nu finns dock de två skriptspråken parallellt fortfarande.

Vad kommer man då kunna göra med Lua och varför ska jag bry mig? Man kommer kunna göra nästan vad som helst. Det kommer bli otroligt kraftfullt och moddar kommer kunna göra saker man inte ens kunnat drömma om i Morrowind. *Minst* lika mycket som nuvarande Morrowind tillsammans med programmet MWSE-Lua  (en skriptextender till vanliga Morrowind som "hackas" in i spelet, OpenMW kan inte köra detta) kan göra just nu, men troligen mycket mer. Vill du programmera ett system för att ha fotbollsmatcher i Morrowind? Det kommer antagligen gå. Varför man nu skulle vilja det...

Men innan man ropar hej för mycket så är det bra att vara medveten om att det än så länge bara är grunden som är lagd för Lua. OpenMWs hårdkodade spelmekanik, grafiska gränssnitt och annat kommer med tiden att bindas över till skript istället för C++ så att allt kommer kunna ändras med nedladdningsbara skriptmoddar. Men vi är inte där riktigt än.

När nästa officiella OpenMW-version är släppt ska jag se till att göra ett officiellt nyhetsinlägg på OpenMWs hemsida om Lua. Jag kanske går in mer på detalj där då.

Hörs!
