+++
title = "Om mig"
translationKey = 'about'
date = "2022-01-20"
[ author ]
  name = "Jocke Berg"
+++

Hej!

Jag heter Jocke och jag har av någon anledning en hemsida. Till vardags jobbar jag som röntgensjuksköterska (jag tar röntgenbilder alltså) på Hudiksvalls sjukhus och på fritiden tar jag mest hand om min son, bakar surdegsbröd eller fixar med huset.

Mina intressen ligger i en blandad kompott bestående av av miljöfrågor, musik, odling, hermetisk konservering/inkokning, historia, runor och ja… alldeles för mycket för att hinna med allt helt enkelt. Jag har också ett stort intresse av ett datorspelsrelaterat projekt som heter OpenMW. Läs mer om det i ett av mina [blogginlägg]({{< ref "/blogg/openmw-en-introduktion.md" >}}) om du är nyfiken.

När den här hemsidan skulle göras valde jag att göra den med en så kallad static site generator. Då kom det liksom en blogg “på köpet”. Vi får väl se om jag skriver mer än bara några testinlägg så småningom. Det kommer nog i så fall handla om väldigt spridda saker, så det blir svårt att hitta en målgrupp för bloggen som består av fler än typ jag själv. Men gillar du lite av det jag nämnde ovan så kommer du säkert hitta ett och annat kul att läsa.

Trevligt att du tittade in!
