+++
title = "About me"
translationKey = 'about'
date = "2022-01-20"
[ author ]
  name = "Jocke Berg"
+++

Hello and welcome!

My name is Joakim, and for some reason I have a website. I work as a radiographer, or "radiology technologist" as they say in the States, at my local hospital. When I'm not doing diagnostic imaging on patients, I am mostly at home taking care of my toddler, making sourdough bread or fixing stuff on our 19th century log house. I also spend quite some time engaging in a project called [OpenMW](https://www.openmw.org/), which is a project to make a new free and open source game engine that runs the now 20 year old (!) RPG called Morrowind.

I have a blog here because the static site generator I use (Hugo) has native support for blogging so why not. So far the blog is only on the Swedish part of the site and it will probably stay that way.

Thanks for stopping by!
